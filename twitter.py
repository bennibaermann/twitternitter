# A mattermost bot which redirects twitter to nitter

# Needs file `mmpy_bot_settings.py` in python path (e.g. usual same directory )
# Should contain settings similar to https://mmpy-bot.readthedocs.io/en/latest/settings.html

from mmpy_bot.bot import listen_to, Bot, respond_to
import re


@listen_to('https://(.*\.)?twitter.com/(.*)')
def give_me(message, sub, urlpart):
    message.reply('https://nitter.net/%s' % urlpart)

@respond_to(".*")
def hi(message):
	pass

if __name__ == "__main__":
    Bot().run()
