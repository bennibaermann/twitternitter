# README

A simple bot based on https://mmpy-bot.readthedocs.io/ that rewrites twitter links to nitter. This way, the content of the tweet can be shown by the built-in mattermost link previewer.

Please copy the `mmpy_bot_settings.sample.py` file to `mmpy_bot_settings.py` and change the fields within.

## Requirements

  pip install -U mmpy_bot
